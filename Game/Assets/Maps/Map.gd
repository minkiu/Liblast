extends Node3D

func update_reflection_probes(var_name, value: bool) -> void:
	if var_name== "render_refprobes_enabled":
		get_node("ReflectionProbes").visible = value
	
# Called when the node enters the scene tree for the first time.
func _ready():
	Settings.var_changed.connect(update_reflection_probes)
	var tmpvar = Settings.get_var("render_refprobes_enabled")
	if tmpvar:
		get_node("ReflectionProbes").visible = tmpvar

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
