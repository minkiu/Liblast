extends AudioStreamPlayer

#@export var minimum_speed: float
#@export var minimum_speed_hysteresis: float
#@export var maximum_speed: float
@export var speed_to_volume: Curve
@export var speed_to_cutoff: Curve

# for mapping the curves
@export var max_speed: float = 100.0

# for pausing playback to save resources and avoid quiet hum all the time
@export var min_speed: float = 2.0
@export var min_speed_hysteresis: float = 0.5

@onready var audio_bus = AudioServer.get_bus_index("WindSFX")
@onready var audio_filter : AudioEffectFilter = AudioServer.get_bus_effect(audio_bus,0)

func _ready() -> void:
	speed_to_volume.bake()
	speed_to_cutoff.bake()

func _physics_process(delta) -> void:
	# make sure the sound is only played if this is the current player
	
	if get_multiplayer_authority() == get_tree().multiplayer.get_unique_id(): # if the player is looking through our eyes
		if playing == false:
			playing = true
	else:
		if playing == true:
			playing = false
	
	var speed = get_parent().get_parent().velocity.length()

	if speed > min_speed + min_speed_hysteresis / 2:
		stream_paused = false
	elif speed < min_speed - min_speed_hysteresis / 2:
		stream_paused = true

	volume_db = speed_to_volume.interpolate_baked(speed / max_speed)
	
	audio_filter.cutoff_hz = speed_to_cutoff.interpolate_baked(speed / max_speed) * 10 # cnvert to 0-1 kHz range
