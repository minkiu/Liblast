extends Node3D

const SPEED: float = 250

func _ready() -> void:
	#translate_object_local(Vector3.FORWARD * SPEED / 60 * randf_range(0, 1)) # randomize starting point
	$RayCast3D.target_position = - Vector3.FORWARD * SPEED  / 30

func _process(delta) -> void:
	if $RayCast3D.is_colliding():
		queue_free()
	else:
		translate_object_local(Vector3.FORWARD * SPEED * delta)
