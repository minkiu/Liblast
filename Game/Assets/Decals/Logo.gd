extends Decal

@onready var main = get_tree().root.get_node("Main")

func _process(delta) -> void:
	emission_energy = sin(main.uptime * 2) + 1
