extends Control

@onready var main = get_tree().get_root().get_node("Main")
@onready var chat_history = $VBoxContainer/ChatHistory
@onready var chat_typing = $VBoxContainer/Typing
@onready var chat_editor = $VBoxContainer/Typing/Editor
@onready var chat_label = $VBoxContainer/Typing/Label

enum ChatState {INACTIVE, TYPING_ALL, TYPING_TEAM}

var state = ChatState.INACTIVE :
	set(new_state):
		state = new_state
		match new_state:
			ChatState.INACTIVE:
				chat_typing.hide()
				chat_editor.release_focus()
			ChatState.TYPING_ALL:
				chat_label.text = "all: "
				chat_typing.show()
				chat_editor.grab_focus()
				chat_editor.text = ''
			ChatState.TYPING_TEAM:
				chat_label.text = "team: "
				chat_typing.show()
				chat_editor.grab_focus()
				chat_editor.text = ''

func _unhandled_input(_event) -> void:
	if state == ChatState.INACTIVE:
		if Input.is_action_just_pressed("say_all"):
			main.focus = main.GameFocus.CHAT
			state = ChatState.TYPING_ALL
			get_tree().get_root().set_input_as_handled()
			
		if Input.is_action_just_pressed("say_team"):
			main.focus = main.GameFocus.CHAT
			state = ChatState.TYPING_TEAM
			get_tree().get_root().set_input_as_handled()
			
	elif Input.is_action_just_pressed("say_cancel"):
			main.focus = main.GameFocus.GAME
			state = ChatState.INACTIVE
	
	if state != ChatState.INACTIVE:
		get_tree().get_root().set_input_as_handled()

# doesn't work over network due to missing RPC implementation in Godot 4
@rpc(call_local, any_peer, reliable) func chat_message(sender_pid: int, recipient_team, message: String) -> void:
	var sender_info = main.player_list.get_item(sender_pid)
	chat_history.append_text('\n' + '[b][color=' + sender_info.color.to_html() +']' + str(sender_info.name) + '[/color][/b] : [i]' + message + '[/i]')
	$Message.play()

@rpc(call_local, any_peer, reliable) func chat_notification(message: String) -> void:
	chat_history.append_text('\n · ' + '[i]' + message + '[/i]')
	$Message.play()

func _on_Editor_text_submitted(new_text: String) -> void:
	# RPC is currently not implemented in the engine
	var sender_id = get_tree().multiplayer.get_unique_id()
	var new_message = [sender_id, 0, new_text]
	rpc(&'chat_message',sender_id, 0, new_text)
	chat_editor.text = ""
	state = ChatState.INACTIVE
	main.focus = main.GameFocus.GAME
