extends Node

### This singleton manages global game settings

# SETTINGS

signal var_changed(var_name, value)

var settings = {} # current game settings

var settings_dir = "user://settings/"
var settings_file = settings_dir + "settings.liblast"
var settings_last = {} # copy of last settings for undo

var presets_dir = "res://Settings/"
var presets = {} # a dictionary of var_name presets. preset : settings{}

var dirty = false # have the settings been altered?

# DEFAULTS

var settings_default = {
	'player_name' = "player",
	'player_color' = Color.GRAY.to_html(),
	'player_uuid' = OS.get_unique_id(),
	'player_first_run' = true,
	'player_play_time' = 0.0,
	'player_games_played' = 0,
	'player_games_won' = 0,
	'player_account_enabled' = false,
	'player_account_login' = null, # encrypted
	'player_account_password_remember' = true,
	'player_account_password' = null, # encrypted
	'player_account_last_login' = null, # timestamp
	'input_mouse_sensitivity' = 0.5,
	'network_game_host' = 'unfa.xyz',
	'network_game_port' = 12597,
	'network_lobby_host' = 'unfa.xyz',
	'network_lobby_port' = 12598,
	'network_auth_host' = 'unfa.xyz',
	'network_auth_port' = 12599,
	'display_fullscreen' = true,
	'display_window_size' = Vector2(1280,720),
	'display_vsync_enabled' = true,
	'display_vsync_mode' = 2, # Disabled, Enabled, Adaptive, Mailbox
	'render_scale' = 3, # 3D Scaling
	'render_msaa' = 0, # none, 2x, 4x, 8x
	'render_ssaa' = 0, # none, fxaa
	'render_debug_mode' = 0,
	'render_debanding_enabled' = false,
	'render_ssrl_enabled' = false,
	'render_ssrl_amount' = 0.25,
	'render_ssrl_limit' = 0.18,
	'render_fsr_enable' = false,
	'render_glow_enabled' = true,
	'render_glow_quality' = 1.0,
	'render_refprobes_enabled' = true,
	'render_ssr_enabled' = false,
	'render_ssr_quality' = 1.0,
	'render_ssao_enabled' = false,
	'render_ssao_quality' = 1.0,
	'render_ssil_enabled' = false,
	'render_ssil_quality' = 1.0,
	'render_particles_extra' = false,
	'render_casing' = true,
	'host_name' = "Liblast Server",
	'host_welcome_message' = "Welcome to Liblast Server! Have fun!",
	'host_peer_limit' = 32,
	'host_peer_require_auth' = false, # players need to be authenticated to join
	}

func _ready() -> void:
	print_debug("Initialising a new settings manager: ", self)
	# ensure the settings directory exists
	var dir = Directory.new()
	if not dir.dir_exists(settings_dir):
		dir.make_dir_recursive(settings_dir)

	var file = File.new()

	settings = null

	if file.file_exists(settings_file):
		print_debug("settings file exists, loading")
		settings = load_settings(settings_file)
		if settings == null:
			print("settings file cannot be read, using defaults")

	if settings == null:
		print_debug("no settings file, using defaults")
		settings = settings_default

	call_apply_all()

# SAVE/LOAD

func save_settings(settings, filename, force = false):
	if not dirty and not force:
		print_debug("Attempted to save unmodified settings, skipping")
		return
	elif not dirty and force:
		print_debug("Forced saving unmodified settings")
	else:
		print_debug("Saving dirty settings")

	var json = JSON.new()
	var json_string = json.stringify(settings)

	var file = File.new()
	file.open(filename, File.WRITE)
	file.store_string(json_string)
	if file.get_error():
		return file.get_error()
	file.close()

	dirty = false

func load_settings(filename):
	var file = File.new()
	var json_string : String
	file.open(filename, File.READ)
	json_string = file.get_as_text()
	if file.get_error():
		return null
	file.close()

	var json = JSON.new()
	json.parse(json_string)
	return json.get_data()

# SET/GET

func set_var(var_name: String, value: Variant):
	if not dirty:
		dirty = true
	settings[var_name] = value
	emit_signal(&'var_changed', var_name, value)
	call_apply_var(var_name)
	save_settings(settings, settings_file)
	print_debug("Variable ", var_name, " was set to ", value)

func get_var(var_name: String): # return a given var_name
	return settings.get(var_name)

# APPLY

func call_apply_var(var_name: String):# call function corresponding to the given var_name
	var apply_method: StringName = StringName("apply_" + var_name)
	if has_method(apply_method):
		call(apply_method, settings[var_name])
	else:
		printerr("Settings var_name ", var_name, " has no apply method")

func call_apply_all(): # apply all current settings
	for key in settings.keys():
		call_apply_var(key)

func load_preset(preset: String): # load var_names from a preset
	settings_last = settings
	settings = presets[preset]

func restore_last():
	settings = settings_last

### VARIABLE APPLY FUNCTIONS

func apply_player_name(value:String) -> void:
		print_debug("Setting player name to ", value)

func apply_display_fullscreen(value:bool) -> void:
	if value:
		get_viewport().mode = Window.MODE_FULLSCREEN
	else:
		get_viewport().mode = Window.MODE_MAXIMIZED

func apply_render_scale(value) -> void:
	var exp_value = pow(2, value) * 0.125
	print_debug("Applying render scale: ", exp_value)
	get_viewport().scaling_3d_scale = exp_value

#func apply_render_refprobes_enabled(value) -> void:
#	print_debug("Applying refprobes enabled: ", value)
#	get_viewport().scaling_3d_scale = exp_value
#

func apply_display_vsync_mode(value) -> void:
	print_debug("Applying Vsync mode: ", value)
	DisplayServer.window_set_vsync_mode(value)

func apply_render_debug_mode(value) -> void:
	get_viewport().debug_draw = value

func apply_render_fsr_enabled(value) -> void:
	get_viewport().scaling_3d_mode = Viewport.SCALING_3D_MODE_FSR if value else Viewport.SCALING_3D_MODE_BILINEAR
