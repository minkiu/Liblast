extends "res://Assets/Menu/MenuData.gd"

#func set_data(_data):
#	super.set_data(_data)
#	$Slider.value = _data

func on_value_changed():
	$Slider.value = value

func on_label_changed():
	$Label.text = label

func on_slider_value_changed(value):
	set_var(value)

func _on_Slider_mouse_entered():
	$HoverSound.play()
