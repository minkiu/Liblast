extends "res://Assets/Menu/MenuData.gd"

var unapplied_text : String

func on_value_changed():
	$LineEdit.text = value

func on_label_changed():
	$Label.text = label

func _on_line_edit_text_submitted(new_text):
	set_var(new_text)

func _on_line_edit_text_changed(new_text):
	unapplied_text = new_text

func _on_line_edit_focus_exited():
	set_var(unapplied_text) # set value after loosing focus
