@tool
extends Control

@export var label = "":
	set(_label):
		label = _label
		on_label_changed()
	get:
		return label

func on_label_changed() -> void:
	self.text = label


func _ready() -> void:
	on_label_changed()
