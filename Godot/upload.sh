#!/bin/bash
VERSION=$(./godot --version)

echo "Godot build version: $VERSION"
LINE="Current Godot version used in \`main\` branch is \`$VERSION\` (taken from https://hugo.pro/projects/godot-builds/[Calinou]):"

sed -i "s|^Current Godot version used.*|$LINE|" ../README.adoc

git add ../README.adoc
git commit -m "Updated Godot version to $VERSION"

rsync -av --progress godot-* unfa.xyz:~/www_liblast/godot/ && git pull && git push
